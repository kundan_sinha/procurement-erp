import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { SupplierService } from './../supplier.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ProductService } from '../../../ProductMaster/Products/product.service';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit {
supplier: any;
addSupplier: any;
product: any;
pro: any;

isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;


  addSupplierForm = new FormGroup({
    payment_term: new FormControl(''),
    duration: new FormControl('')
  });
  constructor(
    private dialogRef: MatDialogRef<AddSupplierComponent>,
    private supplierservice: SupplierService,
    private productservice: ProductService,
    private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.productservice.getProducts()
    .subscribe(response => {
      this.product = response.json();
      console.log(this.product);
        });

    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this.formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
  }
  onSubmit() {
    this.supplierservice.addSupplier(this.addSupplierForm.value).subscribe(response => {
      this.dialogRef.close();
        });
    }
    filter(query) {
      this.supplierservice.filter(query)
    .subscribe(response => {
      this.pro = response.json().data;
    });
  }
}
