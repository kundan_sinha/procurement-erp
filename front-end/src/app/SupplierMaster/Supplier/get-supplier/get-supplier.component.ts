import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../supplier.service';
import { AddSupplierComponent } from '../add-supplier/add-supplier.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-get-supplier',
  templateUrl: './get-supplier.component.html',
  styleUrls: ['./get-supplier.component.css']
})
export class GetSupplierComponent implements OnInit {
supplier: any;
addsupplier: any;

  constructor(
    private service: SupplierService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.service.getSupplier()
    .subscribe(response => {
    this.supplier = response.json();
    console.log(this.supplier);
      });
  }
  add() {
    this.dialog.open(AddSupplierComponent)
    .afterClosed()
    .subscribe(result => {
      this.addsupplier = result;
      // this.supplier.push({
      //   Id: this.supplier.length + 1,
      //   payment_term: this.addsupplier.json().term,
      //   payment_duration: this.addsupplier.json().duration
      // });
    });
    }
}
