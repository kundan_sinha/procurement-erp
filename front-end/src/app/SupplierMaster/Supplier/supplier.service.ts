import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SupplierService {
private url = 'http://localhost:3000';

constructor(private http: Http) { }

getSupplier() {
  return this.http.get(this.url + '/supplier');
 }

 addSupplier(value) {
  return this.http.post(this.url + '/addSupplier', value);
}

filter(query): Observable<any> {
  return this.http.get(this.url + '/search?key=' + query);
}
}
