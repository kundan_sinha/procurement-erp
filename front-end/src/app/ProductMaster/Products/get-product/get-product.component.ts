import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddProductComponent } from '../add-product/add-product.component';
import { UpdateProductComponent } from '../update-product/update-product.component';

@Component({
  selector: 'app-get-product',
  templateUrl: './get-product.component.html',
  styleUrls: ['./get-product.component.css']
})
export class GetProductComponent implements OnInit {
  product: any;
  addproduct: any;

  constructor(
    private service: ProductService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.service.getProducts()
    .subscribe(response => {
    this.product = response.json();
      });
  }

  add() {
    this.dialog.open(AddProductComponent)
    .afterClosed()
    .subscribe(result => {
      this.addproduct = result;
      this.product.push({
        productId: 'P000' + (this.product.length + 1),
        product_name: this.addproduct.json().name,
        product_group: this.addproduct.json().group,
        product_grade: this.addproduct.json().grade,
        product_uom: this.addproduct.json().uom,
        product_taxrate: this.addproduct.json().taxrate,
        product_hsn: this.addproduct.json().hsn
      });
    });
    }

  onDelete(id, index) {
      if (confirm('You sure you want to delete?')) {
      this.product.splice(index, 1);
      this.service.deleteProduct(id)
      .subscribe(response => {});
        }
  }

  onUpdate(id) {
    localStorage.setItem('id', id);
    this.dialog.open(UpdateProductComponent)
    .afterClosed()
    .subscribe(result => {});
  }
}
