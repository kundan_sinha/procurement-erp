import { UomService } from './../../UOM/uom.service';
import { FormGroup, FormControl } from '@angular/forms';

import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { ProductService } from 'src/app/ProductMaster/Products/product.service';
import { GroupService } from '../../Group/group.service';
import { TaxService } from '../../Tax/tax.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
groups: any;
uoms: any;
taxs: any;

  addProductForm = new FormGroup({
    product_name: new FormControl(''),
    product_group: new FormControl(''),
    product_grade: new FormControl(''),
    product_uom: new FormControl(''),
    product_taxrate: new FormControl(''),
    product_hsn: new FormControl(''),
  });

  constructor(
    private dialogRef: MatDialogRef<AddProductComponent>,
    private productservice: ProductService,
    private groupservice: GroupService,
    private uomservice: UomService,
    private taxservice: TaxService
  ) { }

  ngOnInit() {
    this.groupservice.getGroups()
    .subscribe(response => {
      this.groups = response.json();
        });
      this.uomservice.getUoms()
        .subscribe(response => {
          this.uoms = response.json();
            });
      this.taxservice.getTaxs()
        .subscribe(response => {
          this.taxs = response.json();
            });
  }
  onSubmit() {
    this.productservice.addProduct(this.addProductForm.value).subscribe(response => {
      this.dialogRef.close(response);
        });
    }

}
