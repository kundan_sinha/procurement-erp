import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { GroupService } from '../../Group/group.service';
import { UomService } from '../../UOM/uom.service';
import { TaxService } from '../../Tax/tax.service';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
id: any;
product: any;
groups: any;
taxs: any;
uoms: any;

UpdateForm = new FormGroup({
  name: new FormControl(''),
  group: new FormControl(''),
  grade: new FormControl(''),
  uom: new FormControl(''),
  tax_rate: new FormControl(''),
  hsn: new FormControl('')
});

  constructor(
    private dialogRef: MatDialogRef<UpdateProductComponent>,
    private service: ProductService,
    private groupservice: GroupService,
    private uomservice: UomService,
    private taxservice: TaxService
  ) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.service.getProduct(this.id)
      .subscribe(response => {
        this.product = response.json();
      });
      this.groupservice.getGroups()
    .subscribe(response => {
      this.groups = response.json();
        });
      this.uomservice.getUoms()
        .subscribe(response => {
          this.uoms = response.json();
            });
      this.taxservice.getTaxs()
        .subscribe(response => {
          this.taxs = response.json();
            });
  }

  onSubmit() {
      this.service.updateProduct(this.id, this.UpdateForm.value, ).subscribe(response => {
        this.dialogRef.close();
          });
        }
  }

