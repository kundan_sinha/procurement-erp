import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url = 'http://localhost:3000';
  constructor(private http: Http) { }

  getProducts() {
    return this.http.get(this.url + '/product');
   }

   getProduct(id) {
    return this.http.get(this.url + '/product' + '/' + id);
   }

   addProduct(value) {
     return this.http.post(this.url + '/addProduct', value);
   }

   deleteProduct(id) {
     return this.http.delete(this.url + '/product' + '/' + id);
   }

   updateProduct(id, value) {
     return this.http.put(this.url + '/product' + '/' + id, value);
   }
}
