import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetUOMComponent } from './get-uom.component';

describe('GetUOMComponent', () => {
  let component: GetUOMComponent;
  let fixture: ComponentFixture<GetUOMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetUOMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetUOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
