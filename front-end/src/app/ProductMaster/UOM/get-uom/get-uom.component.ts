import { UomService } from './../uom.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddUOMComponent } from '../add-uom/add-uom.component';
import { UpdateUOMComponent } from '../update-uom/update-uom.component';

@Component({
  selector: 'app-get-uom',
  templateUrl: './get-uom.component.html',
  styleUrls: ['./get-uom.component.css']
})
export class GetUOMComponent implements OnInit {
uom: any;
adduom: any;
  constructor(
    private service: UomService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.service.getUoms()
    .subscribe(response => {
    this.uom = response.json();
      });
  }
  add() {
    this.dialog.open(AddUOMComponent)
    .afterClosed()
    .subscribe(result => {console.log(result);
      this.adduom = result;
      this.uom.push({
        uomId: this.uom.length + 1,
        uom_name: this.adduom.json().name
      });
     });
    }

    onDelete(id, index) {
      if (confirm('You sure you want to delete?')) {
      this.uom.splice(index, 1);
      this.service.deleteUom(id)
      .subscribe(response => {});
        }
  }

  onUpdate(id) {
    localStorage.setItem('id', id);
    this.dialog.open(UpdateUOMComponent)
    .afterClosed()
    .subscribe(result => {});
  }
}


