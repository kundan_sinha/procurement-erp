import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UomService } from '../uom.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-uom',
  templateUrl: './add-uom.component.html',
  styleUrls: ['./add-uom.component.css']
})
export class AddUOMComponent implements OnInit {

addUomForm = new FormGroup({
    uom_name: new FormControl(''),

  });

  constructor(
    private dialogRef: MatDialogRef<AddUOMComponent>,
    private service: UomService
  ) { }

  ngOnInit() {}
  onSubmit() {
    this.service.addUom(this.addUomForm.value).subscribe(response => {
      this.dialogRef.close(response);
        });
    }
}
