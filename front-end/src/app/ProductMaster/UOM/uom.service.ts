import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UomService {
  private url = 'http://localhost:3000';
  constructor(private http: Http) { }

  getUoms() {
    return this.http.get(this.url + '/uom');
   }

   getUom(id) {
    return this.http.get(this.url + '/uom' + '/' + id);
   }
   addUom(value) {
     return this.http.post(this.url + '/addUom', value);
   }

   deleteUom(id) {
     return this.http.delete(this.url + '/uom' + '/' + id);
   }

   updateUom(id, value) {
     return this.http.put(this.url + '/uom' + '/' + id, value);
   }
}
