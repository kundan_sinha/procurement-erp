import { Component, OnInit } from '@angular/core';
import { UomService } from '../uom.service';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-uom',
  templateUrl: './update-uom.component.html',
  styleUrls: ['./update-uom.component.css']
})
export class UpdateUOMComponent implements OnInit {
id: any;
uom: any;

UpdateForm = new FormGroup({
  name: new FormControl(''),
});

  constructor(
    private dialogRef: MatDialogRef<UpdateUOMComponent>,
    private service: UomService
  ) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.service.getUom(this.id)
    .subscribe(response => {
      this.uom = response.json();
      console.log(this.uom[0].uom_name);
    });
}

onSubmit() {
    this.service.updateUom(this.id, this.UpdateForm.value, ).subscribe(response => {
      this.dialogRef.close();
        });
      }
  }

