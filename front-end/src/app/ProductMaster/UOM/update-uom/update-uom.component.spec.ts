import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUOMComponent } from './update-uom.component';

describe('UpdateUOMComponent', () => {
  let component: UpdateUOMComponent;
  let fixture: ComponentFixture<UpdateUOMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUOMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
