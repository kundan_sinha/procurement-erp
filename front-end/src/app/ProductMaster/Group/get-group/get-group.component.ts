import { Component, OnInit } from '@angular/core';
import { GroupService } from '../group.service';
import { AddGroupComponent } from '../add-group/add-group.component';
import { MatDialog } from '@angular/material';
import { UpdateGroupComponent } from '../update-group/update-group.component';

@Component({
  selector: 'app-get-group',
  templateUrl: './get-group.component.html',
  styleUrls: ['./get-group.component.css']
})
export class GetGroupComponent implements OnInit {
group: any;
addgroup: any;
  constructor(
    private service: GroupService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.service.getGroups()
    .subscribe(response => {
    this.group = response.json();
      });
  }

  add() {
    this.dialog.open(AddGroupComponent)
    .afterClosed()
    .subscribe(result => {console.log(result);
      this.addgroup = result;
      this.group.push({
        groupId: this.group.length + 1,
        group_name: this.addgroup.json().name,
      });
     });
    }


  onDelete(id, index) {
      if (confirm('You sure you want to delete?')) {
      this.group.splice(index, 1);
      this.service.deleteGroup(id)
      .subscribe(response => {});
        }
  }

  onUpdate(id) {
    localStorage.setItem('id', id);
    this.dialog.open(UpdateGroupComponent)
    .afterClosed()
    .subscribe(result => {});
}
  }

