import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private url = 'http://localhost:3000';

  constructor(private http: Http) { }

  getGroups() {
    return this.http.get(this.url + '/group');
   }

   getGroup(id) {
    return this.http.get(this.url + '/group' + '/' + id);
   }

   addGroup(value) {
     return this.http.post(this.url + '/addGroup', value);
   }

   deleteGroup(id) {
     return this.http.delete(this.url + '/group' + '/' + id);
   }

   updateGroup(id, value) {
     return this.http.put(this.url + '/group' + '/' + id, value);
   }
}
