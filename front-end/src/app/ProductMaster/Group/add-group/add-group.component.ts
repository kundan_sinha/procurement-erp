import { FormGroup, FormControl } from '@angular/forms';
import { GroupService } from './../group.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {
  addGroupForm = new FormGroup({
    group_name: new FormControl(''),
  });

  constructor(
    private dialogRef: MatDialogRef<AddGroupComponent>,
    private service: GroupService
  ) { }

  ngOnInit() {}
  onSubmit() {
    this.service.addGroup(this.addGroupForm.value).subscribe(response => {
      this.dialogRef.close(response);
        });
    }

}
