import { Component, OnInit } from '@angular/core';
import { GroupService } from '../group.service';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-group',
  templateUrl: './update-group.component.html',
  styleUrls: ['./update-group.component.css']
})
export class UpdateGroupComponent implements OnInit {
id: any;
group: any;

UpdateForm = new FormGroup({
  name: new FormControl(''),
});
  constructor(
    private dialogRef: MatDialogRef<UpdateGroupComponent>,
    private service: GroupService) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.service.getGroup(this.id)
      .subscribe(response => {
        this.group = response.json();
        console.log(this.group[0].payment_term);
      });
  }
  onSubmit() {
    this.service.updateGroup(this.id, this.UpdateForm.value, ).subscribe(response => {
      this.dialogRef.close();
        });
      }
}
