import { PaymentService } from './../payment.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-payment',
  templateUrl: './update-payment.component.html',
  styleUrls: ['./update-payment.component.css']
})
export class UpdatePaymentComponent implements OnInit {
  id: any;
 payment: any;

  UpdateForm = new FormGroup({
    term: new FormControl(''),
    duration: new FormControl('')
  });

  constructor(
    private dialogRef: MatDialogRef<UpdatePaymentComponent>,
    private service: PaymentService
  ) { }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.service.getPayment(this.id)
      .subscribe(response => {
        this.payment = response.json();
        console.log(this.payment[0].payment_term);
      });
  }

  onSubmit() {
      this.service.updatePayment(this.id, this.UpdateForm.value, ).subscribe(response => {
        this.dialogRef.close();
          });
        }
}
