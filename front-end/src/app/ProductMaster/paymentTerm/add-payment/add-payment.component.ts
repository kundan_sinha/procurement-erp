import { Router } from '@angular/router';
import { PaymentService } from './../payment.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css']
})

export class AddPaymentComponent implements OnInit {

  addPaymentForm = new FormGroup({
    payment_term: new FormControl(''),
    duration: new FormControl('')
  });

  constructor(
    private dialogRef: MatDialogRef<AddPaymentComponent>,
    private service: PaymentService) { }

  ngOnInit() {}
  onSubmit() {
    this.service.addPayment(this.addPaymentForm.value).subscribe(response => {
      this.dialogRef.close(response);
        });
    }

}
