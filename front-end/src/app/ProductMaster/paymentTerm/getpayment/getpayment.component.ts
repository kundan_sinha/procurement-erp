import { AddPaymentComponent } from './../add-payment/add-payment.component';
import { PaymentService } from './../payment.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { UpdatePaymentComponent } from '../update-payment/update-payment.component';

@Component({
  selector: 'app-getpayment',
  templateUrl: './getpayment.component.html',
  styleUrls: ['./getpayment.component.css']
})
export class GetpaymentComponent implements OnInit {
  payment: any;
  addpayment: any;

  constructor(
    private service: PaymentService,
    private dialog: MatDialog,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.service.getPayments()
    .subscribe(response => {
    this.payment = response.json();
      });
  }

  add() {
    this.dialog.open(AddPaymentComponent)
    .afterClosed()
    .subscribe(result => {
      this.addpayment = result;
      this.payment.push({
        paymentId: this.payment.length + 1,
        payment_term: this.addpayment.json().term,
        payment_duration: this.addpayment.json().duration
      });
    });
    }

  onDelete(id, index) {
      if (confirm('You sure you want to delete?')) {
      this.payment.splice(index, 1);
      this.service.deletePayment(id)
      .subscribe(response => {});
        }
  }

  onUpdate(id, payment_term, payment_duration, index) {
    localStorage.setItem('id', id);
    this.dialog.open(UpdatePaymentComponent)
    .afterClosed()
    .subscribe(result => {});
  }
}

