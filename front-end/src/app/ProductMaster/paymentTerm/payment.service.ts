import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class PaymentService {
  private url = 'http://localhost:3000';

  constructor(private http: Http) { }
  getPayments() {
    return this.http.get(this.url + '/payment');
   }

   getPayment(id) {
    return this.http.get(this.url + '/payment' + '/' + id);
   }
   addPayment(value) {
     return this.http.post(this.url + '/addPayment', value);
   }

   deletePayment(id) {
     return this.http.delete(this.url + '/payment' + '/' + id);
   }

   updatePayment(id, value) {
     return this.http.put(this.url + '/payment' + '/' + id, value);
   }
}
