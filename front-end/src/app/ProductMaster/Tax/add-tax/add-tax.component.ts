import { FormGroup, FormControl } from '@angular/forms';
import { TaxService } from './../tax.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-tax',
  templateUrl: './add-tax.component.html',
  styleUrls: ['./add-tax.component.css']
})
export class AddTaxComponent implements OnInit {
  addTaxForm = new FormGroup({
    tax_name: new FormControl(''),
    tax_rate: new FormControl('')
  });

  constructor(
    private dialogRef: MatDialogRef<AddTaxComponent>,
    private service: TaxService
  ) { }

  ngOnInit() {}
  onSubmit() {
    this.service.addTax(this.addTaxForm.value).subscribe(response => {
      this.dialogRef.close(response);
        });
    }
}
