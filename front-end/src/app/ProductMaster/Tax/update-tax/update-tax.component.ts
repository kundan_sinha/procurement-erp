import { TaxService } from './../tax.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-tax',
  templateUrl: './update-tax.component.html',
  styleUrls: ['./update-tax.component.css']
})
export class UpdateTaxComponent implements OnInit {
id: any;
tax: any;

  constructor(
    private dialogRef: MatDialogRef<UpdateTaxComponent>,
    private service: TaxService) { }

    UpdateForm = new FormGroup({
      name: new FormControl(''),
      rate: new FormControl('')
    });

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.service.getTax(this.id)
      .subscribe(response => {
        this.tax = response.json();
        console.log(this.tax[0].tax_name);
      });
  }
  onSubmit() {
    this.service.updateTax(this.id, this.UpdateForm.value, ).subscribe(response => {
      this.dialogRef.close();
        });
      }
}
