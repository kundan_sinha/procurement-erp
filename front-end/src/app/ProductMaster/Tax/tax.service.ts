import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})


export class TaxService {
  private url = 'http://localhost:3000';

  constructor(private http: Http) { }

  getTaxs() {
    return this.http.get(this.url + '/tax');
   }

   getTax(id) {
    return this.http.get(this.url + '/tax' + '/' + id);
   }

   addTax(value) {
     return this.http.post(this.url + '/addTax', value);
   }

   deleteTax(id) {
     return this.http.delete(this.url + '/tax' + '/' + id);
   }

   updateTax(id, value) {
     return this.http.put(this.url + '/tax' + '/' + id, value);
   }
}
