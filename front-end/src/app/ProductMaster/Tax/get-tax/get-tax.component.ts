import { TaxService } from './../tax.service';
import { Component, OnInit } from '@angular/core';
import { AddTaxComponent } from '../add-tax/add-tax.component';
import { MatDialog } from '@angular/material';
import { UpdateTaxComponent } from '../update-tax/update-tax.component';

@Component({
  selector: 'app-get-tax',
  templateUrl: './get-tax.component.html',
  styleUrls: ['./get-tax.component.css']
})
export class GetTaxComponent implements OnInit {
tax: any;
addtax: any;
  constructor(
    private service: TaxService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.service.getTaxs()
    .subscribe(response => {
    this.tax = response.json();
    console.log(this.tax);
      });
  }
  add() {
    this.dialog.open(AddTaxComponent)
    .afterClosed()
    .subscribe(result => {console.log(result);
      this.addtax = result;
      this.tax.push({
        taxId: this.tax.length + 1,
        tax_name: this.addtax.json().name,
        tax_rate: this.addtax.json().rate
      });
     });
    }


  onDelete(id, index) {
      if (confirm('You sure you want to delete?')) {
      this.tax.splice(index, 1);
      this.service.deleteTax(id)
      .subscribe(response => {});
        }
  }

  onUpdate(id) {
    localStorage.setItem('id', id);
    this.dialog.open(UpdateTaxComponent)
    .afterClosed()
    .subscribe(result => {});
}
}
