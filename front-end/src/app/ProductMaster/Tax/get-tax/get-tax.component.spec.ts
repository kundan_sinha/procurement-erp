import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetTaxComponent } from './get-tax.component';

describe('GetTaxComponent', () => {
  let component: GetTaxComponent;
  let fixture: ComponentFixture<GetTaxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetTaxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetTaxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
