import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GroupService } from './ProductMaster/Group/group.service';
import { ProductService } from './ProductMaster/Products/product.service';
import { PaymentService } from './ProductMaster/paymentTerm/payment.service';
import { BrowserModule, DomSanitizer} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { MatDialogModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatBadgeModule,
    MatCardModule,
    MatIconRegistry,
    MatIconModule,
    MatStepperModule,
    MatAutocompleteModule} from '@angular/material';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule,
          NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { AddPaymentComponent } from './ProductMaster/paymentTerm/add-payment/add-payment.component';
import { GetpaymentComponent } from './ProductMaster/paymentTerm/getpayment/getpayment.component';
import { AddProductComponent } from './ProductMaster/Products/add-product/add-product.component';
import { GetProductComponent } from './ProductMaster/Products/get-product/get-product.component';
import { AddUOMComponent } from './ProductMaster/UOM/add-uom/add-uom.component';
import { GetUOMComponent } from './ProductMaster/UOM/get-uom/get-uom.component';
import { UomService } from './ProductMaster/UOM/uom.service';
import { TaxService } from './ProductMaster/Tax/tax.service';
import { AddSupplierComponent } from './SupplierMaster/Supplier/add-supplier/add-supplier.component';
import { GetSupplierComponent } from './SupplierMaster/Supplier/get-supplier/get-supplier.component';
import { AddGroupComponent } from './ProductMaster/Group/add-group/add-group.component';
import { GetGroupComponent } from './ProductMaster/Group/get-group/get-group.component';
import { HeaderComponent } from './NavBar/header/header.component';
import { FooterComponent } from './NavBar/footer/footer.component';
import { NavComponent } from './NavBar/nav/nav.component';
import { AppmenuComponent } from './NavBar/appmenu/appmenu.component';
import { GetTaxComponent } from './ProductMaster/Tax/get-tax/get-tax.component';
import { AddTaxComponent } from './ProductMaster/Tax/add-tax/add-tax.component';
import { HttpModule } from '@angular/http';
import { UpdatePaymentComponent } from './ProductMaster/paymentTerm/update-payment/update-payment.component';
import { UpdateTaxComponent } from './ProductMaster/Tax/update-tax/update-tax.component';
import { UpdateGroupComponent } from './ProductMaster/Group/update-group/update-group.component';
import { UpdateUOMComponent } from './ProductMaster/UOM/update-uom/update-uom.component';
import { UpdateProductComponent } from './ProductMaster/Products/update-product/update-product.component';




@NgModule({
  declarations: [
    AppComponent,
    AddPaymentComponent,
    GetpaymentComponent,
    AddProductComponent,
    GetProductComponent,
    AddUOMComponent,
    GetUOMComponent,
    AddSupplierComponent,
    GetSupplierComponent,
    AddGroupComponent,
    GetGroupComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    AppmenuComponent,
    AddTaxComponent,
    GetTaxComponent,
    UpdatePaymentComponent,
    UpdateTaxComponent,
    UpdateGroupComponent,
    UpdateUOMComponent,
    UpdateProductComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatBadgeModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {path: '', component: AppComponent},
      {path: 'product', component: GetProductComponent},
      {path: 'addProduct', component: AddProductComponent},
      {path: 'updateProduct/:id', component: UpdateProductComponent},
      {path: 'uom', component: GetUOMComponent},
      {path: 'addUom', component: AddUOMComponent},
      {path: 'updateUom/:id', component: UpdateUOMComponent},
      {path: 'group', component: GetGroupComponent},
      {path: 'addGroup', component: AddGroupComponent},
      {path: 'updateGroup/:id', component: UpdateGroupComponent},
      {path: 'tax', component: GetTaxComponent},
      {path: 'addTax', component: AddTaxComponent},
      {path: 'updateTax/:id', component: UpdateTaxComponent},
      {path: 'payment', component: GetpaymentComponent},
      {path: 'addPayment', component: AddPaymentComponent},
      {path: 'updatePayment/:id', component: UpdatePaymentComponent},
      {path: 'supplier', component: GetSupplierComponent},
      {path: 'addSupplier', component: AddSupplierComponent},
  ])
],
  providers: [
    PaymentService,
    ProductService,
    GroupService,
    UomService,
    TaxService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer) {
  matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')); // Or whatever path you placed mdi.svg at
}
}
