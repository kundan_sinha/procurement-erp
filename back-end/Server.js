//////// Imorting all the required packages ////////////
var express=require('express');
var app=express();
var mysql=require('mysql');
var bodyParser = require('body-parser');
const bcrypt = require('bcrypt');


/* to read html details */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('views',__dirname + '/views');
app.use(express.static(__dirname + '/JS'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.listen(3000,function(){
  console.log("We have started our server on port 3000");
  });


// DataBase Connection
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'procurement' //database name
});

/* To set access control*/
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

connection.connect();
console.log('database connected')


/* Registration */
app.post('/registration', (req, res) => {
  let hash = bcrypt.hashSync(req.body.passwordFormGroup.password, 10);
  connection.query('Select email from users', (err, rows, fields) =>{
    for (var i = 0; i<rows.length; i++) {
        if (req.body.email == rows[i].email) {
            code= 200;
            break;
            }
    } 
  }) 
  if (code= 200) {
    res.json({
      code: 400
    })
} else{
      var sql = "INSERT INTO users (name, email, phone, password) VALUES ('"+req.body.name+"','"+req.body.email+"','"+req.body.phone+"','"+hash+"')";
    connection.query(sql, (err, result) => {
      if (err) throw err;
          res.send({
            code: 202
          });
       });
      }
  // var sql = "INSERT INTO users (name, email, phone, password) VALUES ('"+req.body.name+"','"+req.body.email+"','"+req.body.phone+"','"+hash+"')";
  //   connection.query(sql, (err, result) => {
  //     if (err) throw err;
  //         res.send({
  //           "message":"User Registered Successfully"
  //         });
  //      });
});


/* Login */
app.post('/login', (req, res) => {
  var email = req.body.email;
  var password = req.body.password;
  connection.query('SELECT * FROM users WHERE email = ?',[email], function (error, results, fields) {
    if (error) {
      console.log("error ocurred",error);
      res.send({
        "code":400,
        "failed":"error ocurred"
      })
    }else{
      if(results.length > 0) {
        const result = bcrypt.compareSync(password, results[0].password);
        if(result){
          res.send({
            "code":200,
            "success":"login sucessfull"
              });
            }
            else{
              res.send({
                "code":204,
                "success":"Email and password does not match"
                  });
            }
          }
          else{
            res.send({
              "code":205,
              "success":"Email does not exits"
               });
          }
        }
      });
    });

// Auto-Search   
// app.get('/search',(req, res) => {
//   if(req.query.key !== ''){
//   connection.query('SELECT * from products where (product_name like "%'+req.query.key+'%") or (hsn like "%'+req.query.key+'%")', function(err, rows, fields) {
// 	  if (err) throw err;
//     const abc=[];
//     for(i=0;i<rows.length;i++){
//     }
//     res.json({
//       data: rows
//     })
//   })
// }
// })


/* To ADD Supplier into DataBase */
app.post('/addSupplier', (req, res) => {
  console.log(global.a);
  connection.query('SELECT product_id from products where product_name= "'+a+'"', (err, rows, fields) =>{
    if (err) throw err;
    var re = Object.values(JSON.parse(JSON.stringify(rows)))
     re.forEach((v) => {
     console.log(v.product_id);
  var sql = "INSERT INTO suppliers (supplier_id, name, email, phone) VALUES ('"+v.product_id+"','"+req.body.name+"', '"+req.body.email+"', '"+req.body.phone+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    console.log("1 record inserted to supplier");
    res.render('index.html');
      });
    });
  });
  });



app.post('/supp', (req, res) => {
 connection.query('SELECT product_id from products where product_name= "'+req.body.id+'"', (err, rows,fields) =>{
  if (err) throw err;
  var re = Object.values(JSON.parse(JSON.stringify(rows)));
  re.forEach((v)=> { const k=v.product_id;
  console.log(k); 
   connection.query('SELECT name, email, phone from suppliers where supplier_id= "'+k+'"', (err, rows,fields) =>{
    if (err) throw err;
    res.render('testtable',{page_title:"Test Table",data:rows});
    console.log(rows[0].name);
    var re = Object.values(JSON.parse(JSON.stringify(rows)));
    re.forEach((v) => { 
      
      // console.log(v.name);
      // console.log(v.email);
      // console.log(v.phone);
        });
      });
    });
  });
  });


/* To search supplier by product NAME */
app.get('/supplier/:name', (req, res) => {
      connection.query('Select product_id from products where product_name="'+req.params.name+'"', (err, rows, fields) =>{
        if (err) throw err;
        var a = Object.values(JSON.parse(JSON.stringify(rows)))
        a.forEach((v)=> { 
        const b= v.product_id; 
        connection.query('Select name, email, phone from suppliers where supplier_id="'+b+'"', (err, rows, fields) =>{   
        res.end(JSON.stringify(rows));
          });
        });
      });
});


/* Complete Supplier List */
app.get('/supplier', (req, res) => {
    connection.query('Select * from suppliers', (err, rows, fields) =>{
      if (err) throw err;
      res.end(JSON.stringify(rows));
      console.log(rows);
  });
});

/* Supplier List with supplier id */
app.get('/supplier/:id', (req, res) => {
  connection.query('Select * from suppliers where supplier_id="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
});
});











/* Payment Term Route */

/* Add Payment Request */
app.post('/addPayment', (req, res) => {
  connection.query('Select * from payment', (err, rows, fields) =>{
    var length = rows.length+1;
    if (err) throw err;
  var sql = "INSERT INTO payment (paymentId, payment_term, payment_duration) VALUES ('"+length+"','"+req.body.payment_term+"','"+req.body.duration+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    res.json({
        "term": req.body.payment_term,
        "duration": req.body.duration
    })
})
})
})


/* Get Payment Request */
app.get('/payment', (req, res) => {
  connection.query('Select * from payment', (err, rows, fields) =>{
    if (err) throw err;
    res.json(rows);
})
})

/* Get Single Payment Request */
app.get('/payment/:id', (req, res) => {
  connection.query('Select * from payment where payment_id ="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
})
})

/* Update Payment Request */
app.put('/payment/:id', (req, res) => {
  var post  = {
    "payment_term": req.body.term, 
    "payment_duration": req.body.duration
  };
var condition = {payment_id:req.params.id};
connection.query('UPDATE payment SET ? WHERE ?', [post, condition] , function(err, result) {
  res.json({
    "code": 200,
    "message": "Success"
  })
});
})

/* Delete Payment Request */
app.delete('/payment/:id', (req, res) => {
  var sql = 'DELETE FROM payment WHERE payment_id = "'+req.params.id+'"';
  connection.query(sql, function (err, result) {
    if (err) throw err;
    res.json({
      "code": 200,
      "message": "Success"
    })
  });
})



/* Tax Rate */


/*  Retrieve Tax Records */
app.get('/tax', (req, res) => {
  connection.query('Select * from tax', (err, rows, fields) =>{
    if (err) throw err;
    res.json(rows);
})
})

/* Retrieve Single Tax Records */
app.get('/tax/:id', (req, res) => {
  connection.query('Select * from tax where tax_id ="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
})
})

/* Add Tax Rate */
app.post('/addTax', (req, res) => {
  connection.query('Select * from tax', (err, rows, fields) =>{
    var length = rows.length+1;
    if (err) throw err;
  var sql = "INSERT INTO tax (taxId, tax_name, tax_rate) VALUES ('"+length+"','"+req.body.tax_name+"','"+req.body.tax_rate+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    res.json({
        "name": req.body.tax_name,
        "rate": req.body.tax_rate
    })
})
})
})

/* Delete Tax Rate */
app.delete('/tax/:id', (req, res) => {
  var sql = 'DELETE FROM tax WHERE tax_id = "'+req.params.id+'"';
  connection.query(sql, function (err, result) {
    if (err) throw err;
    res.json({
      "code": 200,
      "message": "Success"
    })
  });
})

/* Update Tax Record */
app.put('/tax/:id', (req, res) => {
  var post  = {
    "tax_name": req.body.name, 
    "tax_rate": req.body.rate
  };
var condition = {tax_id:req.params.id};
connection.query('UPDATE tax SET ? WHERE ?', [post, condition] , function(err, result) {
  res.json({
    "code": 200,
    "message": "Success"
  })
});
})



/* Group */


/*  Retrieve Group Records */
app.get('/group', (req, res) => {
  connection.query('Select * from gp', (err, rows, fields) =>{
    if (err) throw err;
    res.json(rows);
})
})

/* Retrieve Group Records */
app.get('/group/:id', (req, res) => {
  connection.query('Select * from gp where group_id ="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
})
})

/* Add Group  */
app.post('/addGroup', (req, res) => {
  connection.query('Select * from gp', (err, rows, fields) =>{
    var length = rows.length+1;
    if (err) throw err;
  var sql = "INSERT INTO gp (groupId, group_name) VALUES ('"+length+"','"+req.body.group_name+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    res.json({
        "name": req.body.group_name
    })
})
})
})

/* Delete Group Record */
app.delete('/group/:id', (req, res) => {
  console.log(req.params.id);
  var sql = 'DELETE FROM gp WHERE group_id = "'+req.params.id+'"';
  connection.query(sql, function (err, result) {
    if (err) throw err;
    res.json({
      "code": 200,
      "message": "Success"
    })
  });
})

/* Update Group Record */
app.put('/group/:id', (req, res) => {
  var post  = {
    "group_name": req.body.name
  };
var condition = {group_id:req.params.id};
connection.query('UPDATE gp SET ? WHERE ?', [post, condition] , function(err, result) {
  res.json({
    "code": 200,
    "message": "Success"
  })
});
})


/* UOM */


/*  Retrieve UOM Records */
app.get('/uom', (req, res) => {
  connection.query('Select * from uom', (err, rows, fields) =>{
    if (err) throw err;
    res.json(rows);
})
})

/* Retrieve UOM Records */
app.get('/uom/:id', (req, res) => {
  connection.query('Select * from uom where uom_id ="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
})
})

/* Add UOM  */
app.post('/addUom', (req, res) => {
  connection.query('Select * from uom', (err, rows, fields) =>{
    var length = rows.length+1;
    if (err) throw err;
  var sql = "INSERT INTO uom (uomId, uom_name) VALUES ('"+length+"','"+req.body.uom_name+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    res.json({
        "name": req.body.uom_name
    })
})
})
})

/* Delete UOM Record */
app.delete('/uom/:id', (req, res) => {
  console.log(req.params.id);
  var sql = 'DELETE FROM uom WHERE uom_id = "'+req.params.id+'"';
  connection.query(sql, function (err, result) {
    if (err) throw err;
    res.json({
      "code": 200,
      "message": "Success"
    })
  });
})

/* Update UOM Record */
app.put('/uom/:id', (req, res) => {
  var post  = {
    "uom_name": req.body.name
  };
var condition = {uom_id:req.params.id};
connection.query('UPDATE uom SET ? WHERE ?', [post, condition] , function(err, result) {
  res.json({
    "code": 200,
    "message": "Success"
  })
});
})




/* Product */


/*  Retrieve Product Records */
app.get('/product', (req, res) => {
  connection.query('Select * from product', (err, rows, fields) =>{
    if (err) throw err;
    res.json(rows);
})
})

/* Retrieve Product Records */
app.get('/product/:id', (req, res) => {
  connection.query('Select * from product where product_id ="'+req.params.id+'"', (err, rows, fields) =>{
    if (err) throw err;
    res.end(JSON.stringify(rows));
})
})

/* Add Product  */
app.post('/addProduct', (req, res) => {
  connection.query('Select * from product', (err, rows, fields) =>{
    var length = rows.length+1;
    var productID = 'P000' + length;
    if (err) throw err;
  var sql = "INSERT INTO product (productId, product_name, product_group, product_grade, product_uom, product_taxrate, product_hsn) VALUES ('"+productID+"','"+req.body.product_name+"','"+req.body.product_group+"','"+req.body.product_grade+"','"+req.body.product_uom+"','"+req.body.product_taxrate+"','"+req.body.product_hsn+"')";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    res.json({
        "name": req.body.product_name,
        "group": req.body.product_group,
        "grade": req.body.product_grade,
        "uom": req.body.product_uom,
        "tax_rate": req.body.product_taxrate,
        "hsn": req.body.product_hsn,
    })
})
})
})

/* Delete Product Record */
app.delete('/product/:id', (req, res) => {
  var sql = 'DELETE FROM product WHERE product_id = "'+req.params.id+'"';
  connection.query(sql, function (err, result) {
    if (err) throw err;
    res.json({
      "code": 200,
      "message": "Success"
    })
  });
})

/* Update Product Record */
app.put('/product/:id', (req, res) => {
  var post  = {
    "product_name": req.body.name,
    "product_group": req.body.group,
    "product_grade": req.body.grade,
    "product_uom": req.body.uom,
    "product_taxrate": req.body.tax_rate,
    "product_hsn": req.body.hsn
  };
var condition = {product_id:req.params.id};
connection.query('UPDATE uom SET ? WHERE ?', [post, condition] , function(err, result) {
  res.json({
    "code": 200,
    "message": "Success"
  })
});
})

app.get('/search',(req, res) => {
  if(req.query.key !== ''){
  connection.query('SELECT * from product where (product_name like "%'+req.query.key+'%") or (productId like "%'+req.query.key+'%")', function(err, rows, fields) {
	  if (err) throw err;
    const abc=[];
    for(i=0;i<rows.length;i++){
    }
    res.json({
      data: rows
    })
  })
}
})
// or (hsn like "%'+req.query.key+'%")